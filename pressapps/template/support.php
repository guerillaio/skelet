<?php 
/**
 * PressApps Support Template page
 * @since 1.0.0
 */
?>


<header class="pa-header">
	<h1><?php _e( 'How can we help?', SK_TEXTDOMAIN ); ?></h1>
	<h3>Ask a question or look for further help below.</h3>
</header>

<ul class="pa-boxes">
	<li>
		<a href="#">
			<i class="sk-icon fa fa-file-text-o"></i>
			<h3><?php _e( 'Documentation', SK_TEXTDOMAIN ); ?></h3>
			<p>Learn how to install, setup and use our products, documentation is bundled with all our premioum products</p>
		</a>
	</li>
	<li>
		<a href="#">
			<i class="sk-icon fa fa-book"></i>
			<h3><?php _e( 'Knowledge Base', SK_TEXTDOMAIN ); ?></h3>
			<p>Articles and screencast on setting up and configuring our products with advanced customisation advise</p>
		</a>
	</li>
	<li>
		<a href="#">
			<i class="sk-icon fa fa-code"></i>
			<h3><?php _e( 'Customization', SK_TEXTDOMAIN ); ?></h3>
			<p>Do you want to customise our product but don't have the time or skills? Let us help we know our products best.</p>
		</a>
	</li>
</ul>

<div class="pa-faq">
    <h2>Frequently Asked Questions</h2>
	<ul>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
	</ul>
	<ul>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
	</ul>
	<ul class="last">
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
		<li>
			<h4 class="pa-question">Instalation and Setup</h4>
			<p class="pa-answer">If you need assistance with a free plugin or theme, please follow the link to the appropriate Support forum on WordPress.org for further assistance.</p>
		</li>
	</ul>
</div>

<div class="pa-cta">
	<h2>Can’t find what you’re looking for?</h2>
	<p>We offer ticket support Monday – Friday between the hours of 7am and 3pm GMT time.</p>
	<p>We reply to every message we receive within 24 hours Monday – Friday. Use our site to create a support ticket, and get help you can read whenever is most convenient.</p>
	<a class="btn-cta" href="#">Open A Ticket</a>
</div>

<div class="pa-feedback">
	<h2>Let Us Know What You Think</h2>
	<p>Your feedback helps us build better products and create better experience for you. Please share you your thoughts and ideas.</p>
	<a class="btn-cta" href="#">Feedback</a>
</div>
