<?php 
/**
 * PressApps Services Template page
 * @since 1.0.0
 */
?>

<header class="pa-header">
	<h1><?php _e( 'Premium Services', SK_TEXTDOMAIN ); ?></h1>
	<h3>Ask a question or look for further help below.</h3>
</header>

<ul class="pa-boxes">
	<li>
		<a href="#">
			<i class="sk-icon fa fa-life-ring"></i>
			<h3><?php _e( 'Pro Support', SK_TEXTDOMAIN ); ?></h3>
			<p>Learn how to install, setup and use our products, documentation is bundled with all our premioum products</p>
		</a>
	</li>
	<li>
		<a href="#">
			<i class="sk-icon fa fa-wrench"></i>
			<h3><?php _e( 'Instalation and Setup', SK_TEXTDOMAIN ); ?></h3>
			<p>Articles and screencast on setting up and configuring our products with advanced customisation advise</p>
		</a>
	</li>
	<li>
		<a href="#">
			<i class="sk-icon fa fa-code"></i>
			<h3><?php _e( 'Customization', SK_TEXTDOMAIN ); ?></h3>
			<p>Do you want to customise our product but don't have the time or skills? Let us help we know our products best.</p>
		</a>
	</li>
</ul>
